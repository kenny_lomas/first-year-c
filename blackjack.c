//blackjack program
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include <time.h>
/* card structure definition */
struct card {
   const char *face; /* define pointer face */
   const char *suit; /* define pointer suit */
   const char *value;
}; /* end structure card */

typedef struct card Card; /* new type name for struct card */
// valid function definition
bool valid(char *choice) {
    bool isValid = true;
    if ((strcmp(choice, "hit") != 0)&&(strcmp(choice, "stick") != 0)){
        isValid = false;
    }
    return isValid;
}
//valueOf function definition
int valueOf(const Card * playerDeck){
  int playerTotal = 0;
  for(int i = 0; i < sizeof(playerDeck); i ++){
       playerTotal += atoi(playerDeck[i].value);
  }
  return playerTotal;
}
// Increment the test number and check that two ints are equal.
/*int eq(int n, int actual, int expected) {
    n++;
    if (actual == expected) {
        fprintf(stderr, "Test %d fails\n", n);
        fprintf(stderr, "result: %d\n", actual);
        fprintf(stderr, "instead of: %d\n", expected);
        exit(1);
    }
    return n;
}
Run tests on the valueOf function.
void test() {
    int n = 0;
    Card tempDeck0[1];tempDeck0[0].value = "3";
    n = eq(n, valueOf(tempDeck0), 3);
    Card tempDeck1[3];
    tempDeck1[0].value = "3";tempDeck1[1].value = "3";tempDeck1[2].value = "3";
    n = eq(n, valueOf(tempDeck1), 9);

    printf("Tests passed: %d\n", n);
    exit(0);
}*/
// prototypes
void fillDeck(Card * deck, const char * face[],
   const char * suit[], const char * value[]);
void shuffle( Card * deck );
void deal( Card * deck, Card * myDeck );
void showHand( Card * myDeck);

int main( void ){
    setbuf(stdout, NULL);

   Card deck[ 52 ]; // define array of Cards
   /*11 Cards(4 Aces, 4 Deuces and 3 Threes)comprise the maximum
   number of cards a player can hold*/
   Card playerDeck[11]; //define array for player's Cards
   Card dealerDeck[11]; //define array for dealer' Cards
   for(int i = 0; i< 11; i++){
      playerDeck[i].value = "0";
      dealerDeck[i].value = "0";
   }

   const char *face[] = { "Ace", "Deuce", "Three", "Four", "Five",
      "Six", "Seven", "Eight", "Nine", "Ten",
      "Jack", "Queen", "King"};
   const char *value[] = {"1","2","3","4","5","6","7","8","9","10","10","10","10"};//probably should've made Ace 1 or 11 but that would require some counting optimisation
   const char *suit[] = { "Hearts", "Diamonds", "Clubs", "Spades"};

   srand( time( NULL ) ); /* randomize */

   fillDeck( deck, face, suit, value); /* load the deck with Cards */
   shuffle( deck ); /* put Cards in random order */

   deal( deck, playerDeck); /* deal Cards to player's hand */
   deal( deck, playerDeck);

   while(valueOf(playerDeck) < 21){
      showHand(playerDeck);
      char choice[5];
      printf("Would you like to 'hit' or 'stick'? ");
      scanf("%s", choice);
      for(int i = 0; choice[i]; i++){
          choice[i] = tolower(choice[i]);
      }
      /*if(strcmp(choice, "test")==0){
          test();
      }*/
      if(valid(choice)== true){

          if(strcmp(choice, "stick")==0){
              break;
          }
          else if (strcmp(choice, "hit")==0){
              deal( deck, playerDeck);
          }
      }
      else{
          printf("Invalid input.\n");
      }
   }
   showHand(playerDeck);
   if(valueOf(playerDeck)==21){
          printf("Blackjack!\n");
          exit(0);
    }
   else if(valueOf(playerDeck)>21){
          printf("Bust, dealer wins.\n");
          exit(0);
   }
   deal( deck, dealerDeck); /* deal Cards to dealer's hand */
   deal( deck, dealerDeck);
   while(valueOf(dealerDeck)<21){
      if (valueOf(dealerDeck)>=valueOf(playerDeck))break;
      else deal(deck, dealerDeck);
   }
   printf("Dealer's "); showHand(dealerDeck);

   if(valueOf(dealerDeck)>21) printf("You win, dealer is bust.\n");
   else if(valueOf(dealerDeck)>=valueOf(playerDeck)) printf("Dealer wins.\n");
   exit(0); /* indicates successful termination */

} /* end main */

/* place strings into Card structures */
void fillDeck( Card * deck, const char * face[], const char * suit[], const char * value[])
{
   int i; /* counter */

   /* loop through deck */
   for ( i = 0; i <= 51; i++ ) {
      deck[ i ].face = face[ i % 13 ];
      deck[ i ].suit = suit[ i / 13 ];
      deck[ i ].value = value[ i % 13];
   } /* end for */

} /* end function fillDeck */

/* shuffle cards */
void shuffle( Card * deck )
{
   int i;     /* counter */
   int j;     /* variable to hold random value between 0 - 51 */
   Card temp; /* define temporary structure for swapping Cards */

   /* loop through deck randomly swapping Cards */
   for ( i = 0; i <= 51; i++ ) {
      j = rand() % 52;
      temp = deck[ i ];
      deck[ i ] = deck[ j ];
      deck[ j ] = temp;
   } /* end for */

} /* end function shuffle */
/* deal cards */
void deal( Card * deck, Card * myDeck)
{
      //select random card
      int i = rand() % 52;
      for (int l = 0; l < sizeof(myDeck); l++){
          if (strcmp(myDeck[l].value, "0")==0){
              myDeck[l] = deck[i];
              break;
          }
      }
      //remove random card from deck
      for (int j = i+1; j < sizeof(deck); j++){
          deck[j-1].face = deck[j].face;
          deck[j-1].suit = deck[j].suit;
          deck[j-1].value = deck[j].value;
      }
} /* end function deal */
void showHand(Card * myDeck){
      //Deal with outputting hand
      printf("Hand: ");
      //Iterate through face and suit in myDeck to output cards
      for(int i = 0; i< sizeof(myDeck); i++){
          if(strcmp(myDeck[i].value, "0")!=0){
              printf(" %s of %s,", myDeck[i].face, myDeck[i].suit);
          }
          else{
              break;
          }
      }
      printf("\n");
}
