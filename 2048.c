//2048 c program
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
struct board {
    char *cells[4][4];
    int moves;
};
typedef struct board Board;
char *B = " ";
// Initialize a blank board for a new game
void newGame(Board *b) {
  b->moves = 0;
  for(int i = 0; i < 4; i++){
    for(int j = 0; j < 4 ; j++){
        b->cells[i][j] = B;
    }
  }
}
void spawn(Board *b) {
  //randomly find blank tile to spawn 2 or 4
  int coords[] = {rand()%4, rand()%4};
  if(strcmp(b->cells[coords[0]][coords[1]],B) == 0){
    if(rand()%2 == 0) b->cells[coords[0]][coords[1]] = "2";
    else b->cells[coords[0]][coords[1]] = "4";
  }
  else spawn(b);
}
char *incrementTwo(char *myCell){
    char *output = myCell;
    /*switch (atoi(myCell)){
        case 2: output = "4";
        case 4: output = "8";
        case 8: output = "16";
        case 16: output = "32";
        case 32: output = "64";
        case 64: output = "128";
        case 128: output = "256";
        case 256: output = "512";
        case 512: output = "1024";
        case 1024: output = "2048";
    }*/
    if(strcmp(output,"2")==0){
      output = "4";
    }
    else if(strcmp(output,"4")==0){
      output = "8";
    }
    else if(strcmp(output,"8")==0){
      output = "16";
    }
    else if(strcmp(output,"16")==0){
      output = "32";
    }
    else if(strcmp(output,"32")==0){
      output = "64";
    }
    else if(strcmp(output,"64")==0){
      output = "128";
    }
    else if(strcmp(output,"128")==0){
      output = "256";
    }
    else if(strcmp(output,"256")==0){
      output = "512";
    }
    else if(strcmp(output,"512")==0){
      output = "1024";
    }
    else if(strcmp(output,"1024")==0){
      output = "2048";
    }
    return output;
}
bool isWon(Board *b){
  bool won = false;
  for(int i = 0; i < 4; i++){
    for(int j = 0; j < 4; j++){
      if(strcmp(b->cells[i][j],"2048")==0){
        won = true;
        break;
      }
    }
  }
  return won;
}
bool isFull(Board *b){
  bool isFull = true;
  for(int i = 0; i < 4; i++){
    for(int j = 0; j < 4; j++){
      if(strcmp(b->cells[i][j],B)==0){
        isFull = false;
        break;
      }
    }
  }
  return isFull;
}

void moveW(Board *b) {
    for(int i = 0; i <=3; i++){
      //match pairs in a line and incrementTwo
      for(int j = 0; j <= 2; j++){
        int pointer = j+1;
        while(pointer<=3){
          if(b->cells[i][pointer] == B) pointer++;
          else if(b->cells[i][pointer]==b->cells[i][j]){
            b->cells[i][j] = incrementTwo(b->cells[i][j]);
            b->cells[i][pointer]=B;
            break;
          }
          else break;
        }
      }
      //move all tiles up to remove blank spaces
      for(int k = 1; k<=3; k++){
        int pointer = k-1;
        while((b->cells[i][pointer]==B)&&(pointer>=0)){
          b->cells[i][pointer] = b->cells[i][pointer+1];
          b->cells[i][pointer+1] = B;
          pointer--;
        }
      }
    }
    b->moves++;
}

void moveS(Board *b) {
  for(int i = 0; i <=3; i++){
    //match pairs in a line and incrementTwo
    for(int j = 3; j >= 1; j--){
      int pointer = j-1;
      while(pointer>=0){
        if(b->cells[i][pointer] == B) pointer--;
        else if(b->cells[i][pointer]==b->cells[i][j]){
          b->cells[i][j] = incrementTwo(b->cells[i][j]);
          b->cells[i][pointer]=B;
          break;
        }
        else break;
      }
    }
    //move all tiles up to remove blank spaces
    for(int k = 2; k>=0; k--){
      int pointer = k+1;
      while((b->cells[i][pointer]==B)&&(pointer<=3)){
        b->cells[i][pointer] = b->cells[i][pointer-1];
        b->cells[i][pointer-1] = B;
        pointer++;
      }
    }
  }
  b->moves++;
}
void moveA(Board *b) {
  for(int j = 0; j <=3; j++){
    //match pairs in a line and incrementTwo
    for(int i = 0; i <= 2; i++){
      int pointer = i+1;
      while(pointer<=3){
        if(b->cells[pointer][j] == B) pointer++;
        else if(b->cells[pointer][j]==b->cells[i][j]){
          b->cells[i][j] = incrementTwo(b->cells[i][j]);
          b->cells[pointer][j]=B;
          break;
        }
        else break;
      }
    }
    //move all tiles up to remove blank spaces
    for(int k = 1; k<=3; k++){
      int pointer = k-1;
      while((b->cells[pointer][j]==B)&&(pointer>=0)){
        b->cells[pointer][j] = b->cells[pointer+1][j];
        b->cells[pointer+1][j] = B;
        pointer--;
      }
    }
  }
  b->moves++;
}
void moveD(Board *b) {
  for(int j = 0; j <=3; j++){
    //match pairs in a line and incrementTwo
    for(int i = 3; i >= 1; i--){
      int pointer = i-1;
      while(pointer>=0){
        if(b->cells[pointer][j] == B) pointer--;
        else if(b->cells[pointer][j]==b->cells[i][j]){
          b->cells[i][j] = incrementTwo(b->cells[i][j]);
          b->cells[pointer][j]=B;
          break;
        }
        else break;
      }
    }
    //move all tiles up to remove blank spaces
    for(int k = 2; k>=0; k--){
      int pointer = k+1;
      while((b->cells[pointer][j]==B)&&(pointer<=3)){
        b->cells[pointer][j] = b->cells[pointer-1][j];
        b->cells[pointer-1][j] = B;
        pointer++;
      }
    }
  }
  b->moves++;
}
// Display the board.
void display(Board b) {
  printf(" \t  %s |  %s |  %s |  %s \n" ,b.cells[0][0], b.cells[1][0], b.cells[2][0], b.cells[3][0]);
  printf(" \t ---+----+----+---- \n");
  printf(" \t  %s |  %s |  %s |  %s \n" ,b.cells[0][1], b.cells[1][1], b.cells[2][1], b.cells[3][1]);
  printf(" \t ---+----+----+---- \n");
  printf(" \t  %s |  %s |  %s |  %s \n" ,b.cells[0][2], b.cells[1][2], b.cells[2][2], b.cells[3][2]);
  printf(" \t ---+----+----+---- \n");
  printf(" \t  %s |  %s |  %s |  %s \n\n" ,b.cells[0][3], b.cells[1][3], b.cells[2][3], b.cells[3][3]);
}
void play(){
  Board bdata = {{{"?","?","?", "?"},{"?","?","?","?"},{"?","?","?","?"},{"?","?","?","?"}},-1};
  Board *board = &bdata;
  newGame(board); spawn(board);
  char in;
  while((isWon(board)== false)&&(isFull(board)==false)){
    spawn(board); display(*board);
    printf("w, a, s or d: \n");
    scanf(" %c", &in);
    getchar();
    if(tolower(in) == 'w'){
      moveW(board);
    }
    else if(tolower(in) == 'a' ){
      moveA(board);
    }
    else if(tolower(in) == 's'){
      moveS(board);
    }
    else if(tolower(in) == 'd'){
      moveD(board);
    }
    else{
      printf("Invalid input\n");
    }
  }
  if(isWon(board)){
    printf("Congratulations! Want to share? You can't!\n");
  }
  else if(isFull(board)){
    printf("GAME OVER.\n");
  }
}


enum type { CHAR, INT, BOOL};
// Check that two ints, chars or bools are equal
int eq(enum type t, int x, int y) {
    static int n = 0;
    n++;
    if (x != y) {
        if (t==CHAR) fprintf(stderr, "Test %d gives %c not %c", n, x, y);
        if (t==INT) fprintf(stderr, "Test %d gives %d not %d", n, x, y);
        if (t==BOOL && x) fprintf(stderr, "Test %d gives true not false", n);
        if (t==BOOL && y) fprintf(stderr, "Test %d gives false not true", n);
        exit(1);
    }
    return n;
}

// More specific versions of the eq function
int eqc(char x, char y) { return eq(CHAR, x, y); }
int eqi(int x, int y) { return eq(INT, x, y); }
int eqb(bool x, bool y) { return eq(BOOL, x, y); }

void test() {
    Board bdata = {{{"?","?","?", "?"},{"?","?","?","?"},{"?","?","?","?"},{"?","?","?","?"}},-1};
    Board *board = &bdata;

    // Tests 1 to 4 (new board)
    newGame(board);
    eqc(*board->cells[0][0], *B);
    eqc(*board->cells[1][2], *B);
    eqc(*board->cells[2][1], *B);
    eqi(board->moves, 0);

    //Tests 5 to 8 (isWon and isFull)
    *board = (Board){{{"2048",B,B,B},{B,B,B,B},{B,B,B,B},{B,B,B,B}},11};
    eqb(isWon(board), true);
    eqb(isFull(board), false);
    *board = (Board){{{"64","4","32", "2"},{"16","8","2","16"},{"4","32","8","4"},{"2","8","4","2"}},13};
    eqb(isWon(board),false);
    eqb(isFull(board), true);

    printf("Tests passed: %d\n", eqi(0, 0) - 1);
}
int main(int n, char *args[n]) {
    setbuf(stdout, NULL);
    srand(time(NULL));
    if (n == 1) test();
    else if (n == 2 && strcmp(args[1], "play")==0) play();
    else {
        fprintf(stderr, "Use: ./2048  OR  ./2048 p \n");
        exit(1);
    }
    return 0;
}
