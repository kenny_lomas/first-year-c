# README #

What is this repository for?

This repository is for the c projects I've worked on in my first year at University. 

I wrote blackjack.c during my second week as an optional extension to that week's 
homework in its current state there is no GUI, in the holiday's I may remedy this.
	
blackjack.c
(triangle.c extension program)

"A gambling card game in which players try to acquire cards with
a face value totaling 21 and no more." - Oxford pocket dictionary

I created a deck of 52 Cards where Card was a defined structure with
attributes: face, suit and value. The program shuffles the deck and
deals 2 cards to the player and dealer (at random). The player is
prompted to enter 'hit' or 'stick', where 'hit' will draw another
card whereas 'stick' will keep the deck and allow the dealer to draw.

____________________________________________________________________________________________________________
2048.c program

Description
Inspired by Gabriele Cirulli game available on the web: http://gabrielecirulli.github.io/2048/.
Combine tiles to build higher tiles; starting with 2s and 4s the aim is to reach 2048, 2^11.

Program structure
Define the structure board with attributes cells and moves.
Define B to represent blank tiles.
Define a newGame function to initialize a blank board for a new game.
Define a spawn function to spawn a 2 or 4 (at random) in a random position.
Define an incrementTwo function to return double the value on a tile.
Define an isWon function to detect a 2048 tile.
Define an isFull function to detect if the board is full.
Define moveW, moveA, moveS and moveD functions to move the tiles in the corresponding direction.
They start with matching pairs closest to the side they are sliding to and then remove blank tiles.
Define a display function to display the board.
Define a play function to use the above functions to allow the user to play.

Testing
There are 8 tests, when the program is given no arguments it runs the tests, but if given the
argument "play" the play function will be executed.

Improvements needed
The UI is too simple to handle the larger numbers, they are displayed but they don't line up.
Some areas in the program are functional but not pretty, for instance the incrementTwo function
treats the tiles as strings and so I continued with this in the definition of the board structure.

Final comment
I am happy with the program, it is functional and my 'move_' functions took multiple attempts to
refine to a satisfying standard.